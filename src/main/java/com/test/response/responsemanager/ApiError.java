package com.test.response.responsemanager;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

/**
 * @author Abhishek
 *
 */

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)

public @Data class ApiError {

	private Instant timeStamp;
	private Integer status;
	private String message;
	private List<String> errors;

	//

	public ApiError() {
		super();
	}

	public ApiError(final Instant timeStamp, final Integer status, final String message, final List<String> errors) {
		super();
		this.timeStamp = timeStamp;
		this.status = status;
		this.message = message;
		this.errors = errors;
	}

	public ApiError(final Instant timeStamp, final Integer status, final String message, final String error) {
		super();
		this.timeStamp = timeStamp;
		this.status = status;
		this.message = message;
		errors = Arrays.asList(error);
	}

	//

	public Integer getStatus() {
		return status;
	}

	public void setStatus(final Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(final List<String> errors) {
		this.errors = errors;
	}

	public void setError(final String error) {
		errors = Arrays.asList(error);
	}

	public Instant getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(final Instant timeStamp) {
		this.timeStamp = timeStamp;
	}

}
