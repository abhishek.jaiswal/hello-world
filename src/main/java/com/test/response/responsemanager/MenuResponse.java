package com.test.response.responsemanager;

/**
 * @author Abhishek
 *
 */

import java.util.Collection;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
public @Data class MenuResponse {

	public MenuResponse() {
		this.requestId = UUID.randomUUID().toString();
	}

	private String requestId;
	private boolean success = false;

	private String message = "";

	// private Collection<ApiResult> results = null;
	private Collection<ApiError> errors = null;

}
