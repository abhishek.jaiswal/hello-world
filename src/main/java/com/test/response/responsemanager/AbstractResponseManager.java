package com.test.response.responsemanager;

/**
 * @author Abhishek
 *
 */

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class AbstractResponseManager {

	protected ResponseEntity<ApiResponse> buildEntityResponse(HttpStatus responseStatus, ApiResponse apiResponse) {
		ResponseEntity<ApiResponse> responseEntity = null;
		if (apiResponse == null) {
			responseEntity = ResponseEntity.status(responseStatus).build();
		} else {
			responseEntity = new ResponseEntity<ApiResponse>(apiResponse, responseStatus);
		}
		return responseEntity;

	}

	protected ResponseEntity<MenuResponse> buildMenuEntityResponse(HttpStatus responseStatus,
			MenuResponse menuResponse) {
		ResponseEntity<MenuResponse> responseEntity = null;
		if (menuResponse == null) {
			responseEntity = ResponseEntity.status(responseStatus).build();
		} else {
			responseEntity = new ResponseEntity<MenuResponse>(menuResponse, responseStatus);
		}
		return responseEntity;

	}
}
