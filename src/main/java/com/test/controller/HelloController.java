package com.test.controller;

/**
 * @author Abhishek
 *
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class HelloController {
	private static Logger logger = LogManager.getLogger(HelloController.class);

	@GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public String healthCheck() {
		return "Looking Good Abhishek!!";

	}

	@GetMapping(path = "/helloWorld", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getHelloWorld() {
		logger.debug("Entering to com.netapp.UserController.getHelloWorld()");
		logger.debug("Exiting to com.netapp.UserController.getHelloWorld()");
		return "Welcome to the CST World";
	}
}
