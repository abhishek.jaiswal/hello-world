package com.test;

/**
 * @author Abhihsek
 *
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class HelloWorld {
	private static Logger logger = LogManager.getLogger(HelloWorld.class);

	public static void main(String[] args) {
		SpringApplication.run(HelloWorld.class, args);
		logger.debug("Sales On Boarding Application API Started..");
	}
}