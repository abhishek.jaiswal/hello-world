/*
 * package com.netapp.configuration;
 * 
 * import java.io.IOException; import java.io.PrintWriter;
 * 
 * import javax.servlet.ServletException; import
 * javax.servlet.http.HttpServletRequest; import
 * javax.servlet.http.HttpServletResponse;
 * 
 * import org.springframework.security.core.AuthenticationException; import
 * org.springframework.security.web.authentication.www.
 * BasicAuthenticationEntryPoint;
 * 
 * public class CustomBasicAuthenticationEntryPoint extends
 * BasicAuthenticationEntryPoint {
 * 
 * @Override public void commence(final HttpServletRequest request, final
 * HttpServletResponse response, final AuthenticationException authException)
 * throws IOException, ServletException { // Authentication failed, send error
 * response. response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
 * response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
 * 
 * PrintWriter writer = response.getWriter(); //writer.println("status: " +
 * response.getHeaderNames()); writer.println("Content Type: " +
 * response.getContentType()); //writer.println("Content Language: " +
 * response.getCharacterEncoding()); writer.println("status: " +
 * response.getStatus()); writer.println("title: " +
 * authException.getMessage());
 * 
 * }
 * 
 * @Override public void afterPropertiesSet() throws Exception {
 * setRealmName("MY_TEST_REALM"); super.afterPropertiesSet(); } }
 */