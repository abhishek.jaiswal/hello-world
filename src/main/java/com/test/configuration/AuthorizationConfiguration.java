/*
 * package com.netapp.configuration;
 * 
 * import javax.servlet.http.HttpServletResponse;
 * 
 * import org.springframework.beans.factory.annotation.Value; import
 * org.springframework.context.annotation.Bean; import
 * org.springframework.context.annotation.Configuration; import
 * org.springframework.http.HttpMethod; import
 * org.springframework.security.config.annotation.authentication.builders.
 * AuthenticationManagerBuilder; import
 * org.springframework.security.config.annotation.method.configuration.
 * EnableGlobalMethodSecurity; import
 * org.springframework.security.config.annotation.web.builders.HttpSecurity;
 * import
 * org.springframework.security.config.annotation.web.builders.WebSecurity;
 * import org.springframework.security.config.annotation.web.configuration.
 * WebSecurityConfigurerAdapter; import
 * org.springframework.security.config.http.SessionCreationPolicy; import
 * org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder; import
 * org.springframework.security.web.AuthenticationEntryPoint; import
 * org.springframework.security.web.authentication.AuthenticationFailureHandler;
 * 
 *//**
	 * @author Abhishek
	 *
	 *//*
		 * 
		 * @Configuration
		 * 
		 * @EnableGlobalMethodSecurity(prePostEnabled = true)
		 * 
		 * public class AuthorizationConfiguration extends WebSecurityConfigurerAdapter
		 * {
		 * 
		 * @Value("${user.oauth.user.username}") private String username;
		 * 
		 * @Value("${user.oauth.user.password}") private String password;
		 * 
		 * private static String REALM = "MY_TEST_REALM";
		 * 
		 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
		 * Exception {
		 * auth.inMemoryAuthentication().withUser("karnesh").password("{noop}test").
		 * roles("Visitor").and()
		 * .withUser(username).password(passwordEncoder().encode(password)).roles(
		 * "Admin"); }
		 * 
		 * @Override protected void configure(HttpSecurity http) throws Exception {
		 * 
		 * http.csrf().disable().authorizeRequests().antMatchers("/Secured/**").hasRole(
		 * "Admin").and().httpBasic()
		 * .realmName(REALM).authenticationEntryPoint(getBasicAuthEntryPoint()); }
		 * 
		 * @Bean public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint() {
		 * return new CustomBasicAuthenticationEntryPoint(); }
		 * 
		 * @Bean public BCryptPasswordEncoder passwordEncoder() { return new
		 * BCryptPasswordEncoder(); } }
		 */